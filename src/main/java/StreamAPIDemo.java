import java.lang.reflect.Array;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StreamAPIDemo<T> extends LambdaDemo {
    public static final Function<List<?>, List<?>> filterNulls = list -> list.stream()
                                                                   .filter(obj -> !Objects.isNull(obj))
                                                                   .collect(Collectors.toList());

    public static final Function<Set<Integer>, Integer> positiveNumbersCount = set -> Math.toIntExact(set.stream()
                                                                                  .filter(number -> number > 0)
                                                                                  .count());

    public static final Function<List<?>, List<?>> last3Elements = list -> list.stream()
                                                                       .skip(Math.max(list.size()-3,0))
                                                                       .collect(Collectors.toList());

    public static final Function<Integer[], List<Integer>> uniqueSquares = array -> Arrays.stream(array)
                                                                                          .map(i -> i*i)
                                                                                          .distinct()
                                                                                          .collect(Collectors.toList());

    public static final Function<List<String>, List<String>> task6 = list -> list.stream()
                                                                                 .filter(str -> str.length() > 0)
                                                                                 .sorted()
                                                                                 .collect(Collectors.toList());

    public static final Function<Set<String>, List<String>> task7 = set -> set.stream()
                                                                              .sorted(Comparator.reverseOrder())
                                                                              .collect(Collectors.toList());

    public static final Function<Set<Integer>, Integer> task8 = set -> set.stream()
                                                                          .map (a -> a*a)
                                                                          .reduce(0, Integer::sum);

    public static final Function<Collection<? extends Human>, Integer> task9 = col -> col.stream()
                                                             .reduce(-1, (cl, hum) -> Math.max(cl,hum.getAge()), Math::max);

    public static final Function<Collection<? extends Human>, List<Human>> task10 = col -> col.stream()
                                                          .sorted(Comparator.comparing(Human::getSex))
                                                          .sorted(Comparator.comparing(Human::getAge))
                                                          .collect(Collectors.toList());



}
