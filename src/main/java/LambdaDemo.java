import java.util.Optional;
import java.util.function.Function;

public class LambdaDemo {
    public static final Function<String, Integer> length = String::length;
    public static final Function<String, Character> maybeFirst = s -> s.length() == 0 ? null : s.charAt(0);
    public static final Function<String, Boolean> withoutSpaces = s -> !s.contains(" ");
    public static final Function<String, Integer> getWordCount = s -> s.split(",").length;
    public static final Function<Human, Integer> humansAge = Human::getAge;

    public static final Function<Human, Function <Human, Boolean>> haveTheSameLastName
            = h1 -> h2 -> h1.getLastName().equals(h2.getLastName());

    public static final Function<Human, String> getFullName
            = h -> h.getFirstName() + " " + h.getMiddleName() + " " + h.getLastName();

    public static final Function<Human, Human> makeOneYearOlder = h ->
    {if (h instanceof Student){
            return new Student ((Student)h).setAge(h.getAge() + 1);}
            else {
        return new Human(h).setAge(h.getAge() + 1); }
    };

    public static final Function<Integer, Function<Human, Function<Human, Function<Human, Boolean>>>> areYounger3Than
            = maxAge -> h1 -> h2 -> h3 -> maxAge > h1.getAge() && maxAge > h2.getAge() && maxAge > h3.getAge();


}
