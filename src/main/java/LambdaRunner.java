import java.util.function.Function;

public class LambdaRunner {

    public static <A, B> B run(Function<A, B> func, A argument){
        return func.apply(argument);
    }
}
