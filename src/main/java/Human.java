import java.util.Objects;

public class Human {

    private String firstName= "Sample Text";
    private String middleName = "Sample Text";
    private String lastName= "Sample Text";


    private Integer age = 0;
    private Sex sex = Human.Sex.Male;
    public enum Sex {Male, Female};

    public Human(String firstName, String middleName, String lastName, Integer age, Sex sex) {
        setFirstName(firstName);
        setMiddleName(middleName);
        setLastName(lastName);

        setAge(age);
        setSex(sex);
    }

    public Human(Human human){
        this(human.getFirstName(), human.getMiddleName(), human.getLastName(), human.getAge(), human.getSex());
    }

    protected String setName (String str){
        if (str.length() == 0){
            throw new IllegalArgumentException("Empty string exception");
        }
        return str;
    }

    public Human setFirstName(String firstName) {
        this.firstName = setName(firstName);
        return this;
    }

    public Human setMiddleName(String middleName) {
        this.middleName = setName(middleName);
        return this;
    }

    public Human setLastName(String lastName) {
        this.lastName = setName(lastName);
        return this;
    }

    public Human setAge(Integer age) {
        if (age < 0) {
            throw new IllegalArgumentException("Negative age exception");
        }

        this.age = age;
        return this;
    }

    public Human setSex(Sex sex) {
        this.sex = sex;
        return this;
    }

    public String getFirstName(){
        return firstName;
    }

    public String getMiddleName(){
        return middleName;
    }

    public String getLastName(){
        return lastName;
    }

    public Integer getAge(){
        return age;
    }

    public Sex getSex(){
        return sex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return firstName.equals(human.firstName) &&
                middleName.equals(human.middleName) &&
                lastName.equals(human.lastName) &&
                age.equals(human.age) &&
                sex == human.sex;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, middleName, lastName, age, sex);
    }
}
