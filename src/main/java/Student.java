import java.util.Objects;

public class Student extends Human {
    private String university = "Sample Text";
    private String department= "Sample Text";
    private String specialisation= "Sample Text";


    public Student(String firstName, String secondName, String lastName, String university, String department, String specialisation, Integer age, Human.Sex sex) {
        super(firstName, secondName, lastName, age, sex);
        this.university = setName(university);
        this.department = setName(department);
        this.specialisation = setName(specialisation);
    }

    public Student(Human human, String university, String department, String specialisation) {
        super(human);
        this.university = setName(university);
        this.department = setName(department);
        this.specialisation = setName(specialisation);
    }

    public Student(Student student){
        super((Human)student);
        this.university = setName(student.getUniversity());
        this.department = setName(student.getDepartment());
        this.specialisation = setName(student.getSpecialisation());
    }

    public Student setUniversity(String str){
        this.university = setName(str);
        return this;
    }

    public Student setDepartment(String str){
        this.department = setName(str);
        return this;
    }

    public Student setSpecialisation(String str){
        this.specialisation = setName(str);
        return this;
    }

    public String getUniversity(){
        return university;
    }

    public String getDepartment(){
        return department;
    }

    public String getSpecialisation(){
        return specialisation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        if (!super.equals(o)) return false;
        Student student = (Student) o;
        return university.equals(student.university) &&
                department.equals(student.department) &&
                specialisation.equals(student.specialisation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), university, department, specialisation);
    }
}
