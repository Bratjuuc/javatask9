import org.junit.Assert;
import org.junit.jupiter.api.Test;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class LambdaDemoTest {
    @Test
    public void maybeFirstTest(){
        Assert.assertEquals((Object) 'a', LambdaDemo.maybeFirst.apply("asdf"));

    }
    @Test
    public void maybeFirstTestNull(){
        Assert.assertNull(LambdaDemo.maybeFirst.apply(""));
    }

    @Test
    public void withoutSpacesTest(){
        Assert.assertTrue( LambdaDemo.withoutSpaces.apply("asdf"));
    }

    @Test
    public void withoutSpacesTest2(){
        Assert.assertFalse(LambdaDemo.withoutSpaces.apply("as df"));
    }

    @Test
    public void getWordCountTest(){
        Assert.assertEquals((Object) 3, LambdaDemo.getWordCount.apply("a, sd, f f f"));
    }

    @Test
    public void haveSameLastNameTest2(){
        Human human = new Human("Functor", "Applicative", "Monad", 33, Human.Sex.Male);
        Human human2 = new Human("Fun2ctor2", "Applic333ative", "Monad", 323, Human.Sex.Female);
        Student student = new Student(human, "OmSU", "IMIT", "Math");

        Assert.assertTrue(LambdaDemo.haveTheSameLastName.apply(student).apply(human2));
    }

    @Test
    public void fullNameTest(){
        Human human = new Human("Functor", "Applicative", "Monad", 33, Human.Sex.Male);

        Assert.assertEquals("Functor Applicative Monad", LambdaDemo.getFullName.apply(human));
    }

    @Test
    public void fullNameTest2(){
        Human human = new Human("Functor", "Applicative", "Monad", 33, Human.Sex.Male);
        Student student = new Student(human, "OmSU", "IMIT", "Math");

        Assert.assertEquals("Functor Applicative Monad", LambdaDemo.getFullName.apply(student));
    }

    @Test
    public void oneYearOlderTest(){
        Human human = new Human("Functor", "Applicative", "Monad", 33, Human.Sex.Male);
        Human human2 = new Human("Functor", "Applicative", "Monad", 34, Human.Sex.Male);

        Assert.assertEquals(human2, LambdaDemo.makeOneYearOlder.apply(human));
    }

    @Test
    public void oneYearOlderTest2(){
        Human human = new Human("Functor", "Applicative", "Monad", 33, Human.Sex.Male);
        Human human2 = new Human("Functor", "Applicative", "Monad", 34, Human.Sex.Male);
        Student student = new Student(human, "OmSU", "IMIT", "Math");
        Student student2 = new Student(human2, "OmSU", "IMIT", "Math");


        Assert.assertEquals(student2, LambdaDemo.makeOneYearOlder.apply(student));
    }

    @Test
    public void youngerThanTest(){
        Assert.assertTrue(LambdaDemo.areYounger3Than.apply(50).apply(humanList[0]).apply(humanList[1]).apply(humanList[2]));
    }

    @Test
    public void lengthTest(){
        Assert.assertEquals(5, (int) LambdaDemo.length.apply("12345"));
    }

    @Test
    public void ageTest(){
        Assert.assertEquals(33, (int) LambdaDemo.humansAge.apply(humanList[0]));
    }

    @Test
    public void youngerThanTest2(){
        Assert.assertFalse(LambdaDemo.areYounger3Than.apply(50).apply(humanList[0]).apply(humanList[1]).apply(humanList[3]));
    }

    Human[] humanList = {new Human("Functor", "Applicative", "Monad", 33, Human.Sex.Male),
            new Human("Functor", "Applicative", "Monad", 33, Human.Sex.Male),
            new Human("Functor", "Applicative", "Monad", 22, Human.Sex.Male),
            new Human("FFFFFF", "AAAAAA", "MMMMM", 313, Human.Sex.Female),
            new Human("ЙЦУКЕН", "ФЫВА", "ЯЧСМИТ", 123, Human.Sex.Female),
            new Human("HUFF", "PUFF", "DUFF", 1001, Human.Sex.Male),
            new Human("Fake", "Orchestra", "Batter", 1, Human.Sex.Male)};
}