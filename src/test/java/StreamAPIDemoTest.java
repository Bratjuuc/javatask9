import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class StreamAPIDemoTest {
    @Test
    public void task1testRegular(){
        List<Integer> input = Arrays.asList(1, null, 2, 3, null, 4,5, null,6);
        Assert.assertEquals(StreamAPIDemo.filterNulls.apply(input), Arrays.asList(1,2,3,4,5,6));
    }
    @Test
    public void task1testEmpty(){
        List<Integer> input = Collections.emptyList();
        Assert.assertEquals(StreamAPIDemo.filterNulls.apply(input), Collections.emptyList());
    }
    @Test
    public void task1testNull(){
        List<Integer> input =Arrays.asList(null, null, null);
        Assert.assertEquals(StreamAPIDemo.filterNulls.apply(input), Collections.emptyList());
    }
    @Test
    public void task2test(){
        Set<Integer> input = new HashSet<>(Arrays.asList(0, 1, 2, -1, -2, 1));
        Assert.assertEquals((long) StreamAPIDemo.positiveNumbersCount.apply(input), 2);
    }
    @Test
    public void last3ElementsTest(){
        List<Integer> list = Arrays.asList(1,2,3,4,5);
        List<Integer> listTail = Arrays.asList(3,4,5);
        Assert.assertEquals(StreamAPIDemo.last3Elements.apply(list), listTail);
    }

    @Test
    public void last3ElementsTest2(){
        List<Integer> list = Arrays.asList(4,5);
        List<Integer> listTail = Arrays.asList(4,5);
        Assert.assertEquals(StreamAPIDemo.last3Elements.apply(list), listTail);
    }

    @Test
    public void last3ElementsTest3(){
        List<Integer> empty = Collections.emptyList();
        Assert.assertEquals(StreamAPIDemo.last3Elements.apply(empty), empty);
    }
    @Test
    public void uniqueSquaresTest(){
        Integer[] list = new Integer[]{1,2,3,3,-3,-3,4,3,3,3,4,5};
        List<Integer> expected = Arrays.asList(1,4,9,16,25);
        Assert.assertEquals(expected, StreamAPIDemo.uniqueSquares.apply(list));
    }

    @Test
    public void task9test(){
        List<Human> humanList = Arrays.asList(new Human("Functor", "Applicative", "Monad", 33, Human.Sex.Male),
                new Human("Functor", "Applicative", "Monad", 33, Human.Sex.Male),
                new Human("Functor", "Applicative", "Monad", 22, Human.Sex.Male),
                new Human("FFFFFF", "AAAAAA", "MMMMM", 313, Human.Sex.Female),
                new Human("ЙЦУКЕН", "ФЫВА", "ЯЧСМИТ", 123, Human.Sex.Female),
                new Human("HUFF", "PUFF", "DUFF", 1001, Human.Sex.Male),
                new Human("Fake", "Orchestra", "Batter", 1, Human.Sex.Male));

        Assert.assertEquals( 1001,(long) StreamAPIDemo.task9.apply(humanList));
    }



}